package BT1;

import java.util.*;

public class Runner {
    public static void main(String[] args) {
        // Bài tạo mảng sinh viên
        List<Student>students=new ArrayList<>();
        Student st1=new Student(1,"AAA","12A1","123",16);
        Student st2=new Student(2,"BBB","12A2","1af23",50);
        Student st3=new Student(3,"CCC","12A3","123f",20);
        Student st4=new Student(4,"DDA","12A1","123fd",25);
        Student st5=new Student(5,"VAA","12A3","123fa",56);
        Student st6=new Student(6,"AFDA","12b1","12fa3",21);
        Student st7=new Student(7,"AGAA","12C1","123afd",45);
        students.add(st1);
        students.add(st2);
        students.add(st3);
        students.add(st4);
        students.add(st5);
        students.add(st6);
        students.add(st7);

        Student st8=new Student(8,"AGAA","12C1","123afd",21);
        addNewStudent(st8,students);

        //so sanh 2 sinh vien
        sosanhAge(st1,st2);

        //convert ArrayList to LinkedList
//        convertArrayListtoLinkedList(students);

        //so sanh time arraylist anh linkedlist
//        sosanhtimeArrayListAndLinkedList();

        //add studen to set
        addStudentToSet(students);
    }

    static void addNewStudent(Student student,List<Student>students){
        int cnt=0;
        for(int i=0;i<students.size();i++){
            if(student.getId()==students.get(i).getId()){
                cnt++;
                break;
            }
        }
        if(cnt==0){
            students.add(student);
        }
        else{
            System.out.println("bi trung id");
        }
    }
    static void sosanhAge(Student a,Student b){
        if(a.getAge()>b.getAge()){
            System.out.println("student 1 co tuoi lon hon student 2");
        }
        else if(a.getAge()==b.getAge()){
            System.out.println("student 1 co tuoi bang student 2");
        }
        else{
            System.out.println("student 1 co tuoi nho hon student 2");

        }
    }
    static void sosanhtimeArrayListAndLinkedList(){
        long time=  System.currentTimeMillis();
        ArrayList<Integer>number=new ArrayList<>();
        LinkedList<Integer>linkedList=new LinkedList<>();
        for(int i=1;i<=100000;i++){
            linkedList.add(1);
        }
        System.out.println(System.currentTimeMillis()-time);
    }
    static void convertArrayListtoLinkedList(List<Student>students){
//        Convert ArrayList<Student>-> LinkedList<Student>
        LinkedList<Student>lists=new LinkedList<>(students);
        for(Student st:lists){
            System.out.println(st);
        }
        ArrayList<Student>arrayList=new ArrayList<>();
//        linkedlist -> arraylist
        arrayList.addAll(lists);
    }

    static void addStudentToSet(List<Student>students){
        HashSet<Student>set=new HashSet<>();
        for(int i=0;i<students.size();i++){
            set.add(students.get(i));
        }
        System.out.println(set.size());
        for(Student s:set){
            System.out.println(s);
        }
        System.out.println("after add");
        Student s1=new Student(9,"adfaf","adsf","123afd",45);
        set.add(s1);
        System.out.println(set.size());
        for(Student s:set){
            System.out.println(s);
        }
    }
}
//https://gitlab.com/hadungth/onehadung
