package Thread.Lesson1;

public class ShareData {
    int rad;

    public ShareData() {
    }

    public ShareData(int rad) {
        this.rad = rad;
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }
}
