package Thread.Lesson1;

public class ThreadSquare extends Thread{
    ShareData shareData;

    public ThreadSquare(ShareData shareData) {
        this.shareData = shareData;
    }

    @Override
    public void run() {

        for(int i=1;i<=10;i++){
           synchronized (shareData){
               try {
                   shareData.notifyAll();
                   shareData.wait();
               } catch (InterruptedException e) {
                   throw new RuntimeException(e);
               }
               int rad=shareData.getRad();
               rad*=rad;
               shareData.setRad(rad);
               System.out.println("T: "+rad);
           }
        }
        synchronized (shareData){
            shareData.notifyAll();
        }
    }
}
