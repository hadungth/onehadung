package Thread.Lesson2;

public class Runner {
    public static void main(String[] args) {
        ShareData shareData=new ShareData();
        ThreadRandom threadRandom=new ThreadRandom(shareData);
        Thread2 thread2=new Thread2(shareData);
        Thread3 thread3=new Thread3(shareData);

        threadRandom.start();
        thread2.start();
        thread3.start();
    }
}
//ls -la xem qua source