package Thread.Lesson2;

import java.util.Random;

public class ThreadRandom extends Thread{
    ShareData shareData;

    public ThreadRandom(ShareData shareData) {
        this.shareData = shareData;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Random random=new Random();
        while(shareData.checkTotal()){
            synchronized (shareData){

                int rad=random.nextInt(100)+1;
                shareData.setRad(rad);
                shareData.plus(rad);
                System.out.println("Thread T1 >>: "+rad);

                if(rad%3==0){
                    shareData.setIndex(2);
                }else{
                    shareData.setIndex(3);
                }

                shareData.notifyAll();
                try {
                    shareData.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        synchronized (shareData){
            shareData.notifyAll();
        }
    }
}
