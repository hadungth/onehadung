package Thread.Lesson2;

public class Thread3 extends Thread{
    ShareData shareData;

    public Thread3(ShareData shareData) {
        this.shareData = shareData;
    }

    @Override
    public void run() {
        while(shareData.checkTotal()){
            synchronized (shareData){
                shareData.notifyAll();
                try {
                    while(shareData.getIndex()!=3&&shareData.checkTotal()){
                        shareData.wait();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                int rad=shareData.getRad();
                if(rad%2==0) {
                    if (rad % 4 == 0) {
                        System.out.println("thread 3>> so chia het cho 2" + rad);
                    } else {
                        System.out.println("so khong chia het cho 4");
                    }
                }else {
                    System.out.println("so le");
                }
                shareData.setIndex(1);
            }
        }
        synchronized (shareData){
            shareData.notifyAll();
        }
    }
}
