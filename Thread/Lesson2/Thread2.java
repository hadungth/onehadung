package Thread.Lesson2;

public class Thread2 extends Thread{
    ShareData shareData;

    public Thread2(ShareData shareData) {
        this.shareData = shareData;
    }

    @Override
    public void run() {
        while(shareData.checkTotal()){
            synchronized (shareData){
                shareData.notifyAll();
                try {
                     while(shareData.getIndex()!=2&&shareData.checkTotal()){
                         shareData.wait();
                     }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                int rad=shareData.getRad();
                if(rad%3==0){
                    rad*=rad;
                    System.out.println("thread 2>>"+rad);
                }
                shareData.setIndex(1);
            }

        }
        synchronized (shareData){
            shareData.notifyAll();
        }
    }
}
