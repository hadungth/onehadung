package Thread.Lesson2;

public class ShareData {
    int rad;
    int total;
    int index;
    public ShareData() {
        this.total=0;
        index=1;
    }

    public ShareData(int rad) {
        this.rad = rad;
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    public synchronized void plus(int value){
        total+=value;
    }
    public synchronized boolean checkTotal(){
        if(total>=200)return false;
        return true;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
