package java_io;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args){
        String pathName="./file_test/input.txt";
        String pathOutput="./file_test/output.txt";
//        FileInputStream inputStream=null;
//        StringBuilder stringBuilder=new StringBuilder();
//        FileOutputStream outputStream=null;
//        try{
//            inputStream=new FileInputStream(pathName);
//            outputStream=new FileOutputStream(pathOutput);
//            int check;
//            while((check=inputStream.read())!=-1){
////                System.out.print((char) check);
//                stringBuilder.append((char)check);
//                outputStream.write((char)check);
//            }
//            outputStream.write("\n".getBytes());
//            outputStream.write("ha van dung".getBytes());
//        }catch (FileNotFoundException exception){
//            System.out.println(exception.getMessage());
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        } finally {
//            if(inputStream!=null){
//                try{
//                    inputStream.close();
//                }catch (IOException exception){
//                    System.out.println(exception.getMessage());
//                }
//            }
//            if(outputStream!=null){
//                try{
//                    outputStream.close();
//                }catch (IOException exception){
//                    System.out.println(exception.getMessage());
//                }
//            }
//        }
//        System.out.println("Ghi file :");
//        System.out.println(stringBuilder.toString());
//
//        try{
//            FileReader fileReader=new FileReader(pathName);
//        }catch (IOException exception){
//            System.out.println(exception.getMessage());
//        }
//    ==========================================================================
        FileInputStream inputStream=null;
        FileOutputStream outputStream=null;
        BufferedInputStream bufferedInputStream=null;
        BufferedOutputStream bufferedOutputStream=null;
        try{
            inputStream=new FileInputStream(pathName);
            bufferedInputStream =new BufferedInputStream(inputStream);
            outputStream=new FileOutputStream(pathOutput);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            byte[] data=new byte[500];
            int length=bufferedInputStream.read(data);
            String result=new String(data,0,length);
            System.out.println(length);
            System.out.println(result);
            outputStream.write(result.getBytes());
        }catch (IOException exception){
            System.out.println(exception.getMessage());
        }finally {
            if(inputStream!=null){
                try{
                    inputStream.close();
                }catch (IOException exception){
                    System.out.println(exception.getMessage());
                }
            }
        }
    }
}
