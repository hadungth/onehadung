package Exception.test;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        try{
            test();
        }catch (AgeException exception){
            System.out.println(exception.getMessage());
        }
    }
    static void test() throws AgeException{
        Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        if(str.length()>=2) {
            throw new AgeException("User", "Id", str);
        }
        System.out.println(str);
    }
}
