package Exception.test;

public class AgeException extends RuntimeException{
    public AgeException(String resource,String fieldName,String fieldValue){
        super("Resource : "+resource+"\nfileName :"+fieldName+"\nfieldValue : "+fieldValue);
    }
}
