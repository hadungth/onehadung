package Exception.SumArray;

import java.util.Scanner;

public class Runner {
    static Scanner sc=new Scanner(System.in);
    public static void main(String[] args) {
        String n_str=sc.nextLine();
        int n=Integer.parseInt(n_str);
        String arrayNumber[]=new String[n+1];
        long sum = 0L;
        try{
            sum=Sum(arrayNumber,n);
            
        }catch (Exception exception){
            System.out.println(exception.getClass().getName());
            System.out.println(exception.getMessage());
        }
        System.out.println("Sum of array:");
        System.out.println(sum);
    }
    static long Sum(String arrayNumber[],int n) throws SumCalcException{
        long sum=0;
        for(int i=1;i<=n;i++)
        {
            arrayNumber[i]=new Scanner(System.in).nextLine();
            long number=0;
            String str=arrayNumber[i];
            for(int j=0;j<str.length();j++){
                char kitu=str.charAt(j);
                if(!(kitu>='0'&&kitu<='9')){
                    throw new SumCalcException("char in number is Invalid");
                }
                else{
                    number=number*10+kitu-'0';
                }
            }
            sum+=(long) number;
        }
        return sum;
    }
}
