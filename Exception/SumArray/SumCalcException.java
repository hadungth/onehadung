package Exception.SumArray;

public class SumCalcException extends RuntimeException{
    public SumCalcException(){
        super();
    }
    public SumCalcException(String message){
        super(message);
    }
}
