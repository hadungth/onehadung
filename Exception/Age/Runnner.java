package Exception.Age;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Runnner {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
//        LocalDate today=LocalDate.now();
//        LocalDate birthday=LocalDate.of(2025,Month.JANUARY,16);
//        Period period=Period.between(birthday,today);
//        String age="age : "+period.getYears()+"\nmonth : "
//                +period.getMonths()+"\ndays :"
//                +period.getDays();
//        System.out.println(age);
        inputDate();

        //tinh next BirthDay
//        nextBirthDay();
    }

    static void inputDate() {
        try{
            System.out.println("Input year:");
            int year=sc.nextInt();
            System.out.println("Input month:");
            int month=sc.nextInt();
            System.out.println("Input day");
            int day=sc.nextInt();
            if(year>2023){
                throw new AgeException("year > 2023");
            }
            if(month>12||month<1){
                throw new AgeException("Invalid month");
            }
            if(day<1||day>31){
                throw new AgeException("Invalid day");
            }
            //tinh age hien tai
            LocalDate today=LocalDate.now();
            LocalDate localDate=LocalDate.of(year,month,day);
            Period period=Period.between(localDate,today);
            if(period.getDays()<0||period.getMonths()<0||period.getYears()<0){
                throw new AgeException("Invalid date < 0");
            }
            String age="year : "+period.getYears()+"\nmonth : "
                    +period.getMonths()+"\nday : "
                    +period.getDays();
            System.out.println(age);
        }catch (InputMismatchException exception){
            System.out.println(exception.getMessage());
        }catch (AgeException exception){
            System.out.println(exception.getMessage());
        }
    }
    static void nextBirthDay(){
        try{
            System.out.println("Input year:");
            int year=sc.nextInt();
            System.out.println("Input month:");
            int month=sc.nextInt();
            System.out.println("Input day");
            int day=sc.nextInt();
            if(year>2023){
                throw new AgeException("year > 2023");
            }
            if(month>12||month<1){
                throw new AgeException("Invalid month");
            }
            if(day<1||day>31){
                throw new AgeException("Invalid day");
            }
            //tinh age hien tai
            LocalDate today=LocalDate.now();
            LocalDate localDate=LocalDate.of(year,month,day);
            LocalDate nextBDay=localDate.withYear(today.getYear());
            //neu sinh nhat cua nam nay da dien ra thi plus year 1
            if(nextBDay.isBefore(today)||nextBDay.isEqual(today)){
                nextBDay=nextBDay.plusYears(1);
            }
            Period period=Period.between(nextBDay,today);
            String nextBirth="year : "+period.getYears()+"\nmonth : "
                    +period.getMonths()+"\nday : "
                    +period.getDays();
            long p= ChronoUnit.DAYS.between(today,nextBDay);
            System.out.println("total :"+p);
        }catch (InputMismatchException exception){
            System.out.println(exception.getMessage());
        }catch (AgeException exception){
            System.out.println(exception.getMessage());
        }
    }
}
//period use age,month,year to now