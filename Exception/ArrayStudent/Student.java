package Exception.ArrayStudent;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Student {
    private Integer id;
    private String name;
    private LocalDate birthDay;

    public Student() {
    }

    public Student(Integer id, String name, String birthDay) {
        this.id = id;
        this.name = name;
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
        this.birthDay=LocalDate.parse(birthDay,formatter);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDay() {
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dateFormat=this.birthDay.format(formatter);
        return dateFormat;
    }

    public void setBirthDay(String birthDay) {
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
        this.birthDay=LocalDate.parse(birthDay,formatter);
    }
    @Override
    public String toString(){
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("id : ").append(this.id)
                .append("\nname : ").append(this.name)
                .append("\nbirthDay :").append(this.getBirthDay());
        return stringBuilder.toString();
    }
}
