package Exception.ArrayStudent;

public class StudentConvertException extends RuntimeException{
    public StudentConvertException(){
        super();
    }
    public StudentConvertException(String message){
        super(message);
    }
}
