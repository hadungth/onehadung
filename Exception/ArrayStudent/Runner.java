package Exception.ArrayStudent;

import java.time.LocalDate;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
       Student student=ConvertStringToStudent("id: 1;name: Mrz Jonh; birthDay: 21/01/1999");
        System.out.println(student);
    }
    static Student ConvertStringToStudent(String str){
        Student student=new Student();
        List<String>list= List.of(str.trim().split(";"));
        try{
            String str_id=cutString(list.get(0),4);
            ConvertId(str_id);
            Integer id=Integer.parseInt(str_id);
            student.setId(id);
        }catch (StudentConvertException exception){
            System.out.println(exception.getMessage());
        }
        String name=cutString(list.get(1),6);
        student.setName(name);
        try{
            String str_day=cutString(list.get(2),11);
            ConvertDay(str_day);
            student.setBirthDay(str_day);
        }catch (StudentConvertException exception){
            System.out.println(exception.getMessage());
        }
        return student;
    }
    static void ConvertId(String str) throws StudentConvertException{
        for(int i=0;i<str.length();i++){
            char kitu=str.charAt(i);
            if(!(kitu>='0'&&kitu<='9')){
                throw new StudentConvertException("id :"+str+": Can't parse a to int");
            }
        }
    }
    static void ConvertDay(String str)throws StudentConvertException{
        for(int i=0;i<str.length();i++){
            char kitu=str.charAt(i);
            if(!(kitu>='0'&&kitu<='9'||kitu=='/')){
                throw new StudentConvertException("day :"+str+": Can't parse a to birthday");
            }
        }
    }
    static String cutString(String str,int index){
        StringBuilder stringBuilder=new StringBuilder();
        for(int i=index;i<str.length();i++){
            stringBuilder.append(str.charAt(i));
        }
        return stringBuilder.toString();
    }
}
