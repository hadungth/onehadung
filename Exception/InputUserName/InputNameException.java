package Exception.InputUserName;

public class InputNameException extends RuntimeException{
    public InputNameException(){
        super();
    }
    public InputNameException(String message){
        super(message);
    }
}
