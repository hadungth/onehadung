package Exception.InputUserName;

import java.util.Scanner;

public class Runner {
    static Scanner sc=new Scanner(System.in);
    public static void main(String[] args) {
        try{
            inputUserName();
            System.out.println("enter a valid name !!!");
        }catch (InputNameException exception){
            System.out.println(exception.getMessage());
        }
    }
    static void inputUserName() throws InputNameException{
        System.out.println("Input userName :");
        String fullName=sc.nextLine();
        fullName=fullName.toLowerCase();
        for(int i=0;i<fullName.length();i++){
            char kitu=fullName.charAt(i);
            if(!(kitu==' '||(kitu>='a'&&kitu<='z'))){
                throw new InputNameException("Invalid name input!...");
            }
        }
    }
}
