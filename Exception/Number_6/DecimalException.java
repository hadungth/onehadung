package Exception.Number_6;

public class DecimalException extends RuntimeException{
    public DecimalException(){
        super();
    }
    public DecimalException(String message){
        super(message);
    }
}
